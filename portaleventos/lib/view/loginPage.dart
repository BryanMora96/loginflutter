import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:portaleventos/app.dart';
import 'package:portaleventos/main.dart';
import 'package:portaleventos/utilities/size_config.dart';
//import 'package:portaleventos/view/viewEvent.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  bool _isLoading = false;

  final TextEditingController emailController = new TextEditingController();
  final TextEditingController passwordController = new TextEditingController();

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.light.copyWith(statusBarColor: Colors.transparent));

    // Size 
    SizeConfig().init(context);

    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
          gradient: LinearGradient(
            colors: [Colors.blue, Colors.teal],
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter
          ),
        ),
        child: _isLoading ? Center(child: CircularProgressIndicator()) : ListView(
          children: <Widget>[
            heardSection(),
            textSection(),
            butonSection(),
            SizedBox(height: 30.0,),
            butonOlviContra(),
          ]
        )
      ),
    );
  }

  signIn(String email, pass) async{

    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    Map data= {
      'email': email.trim(),
      'password': pass
    };
    print(data);

    var jsonResponse = null;
    var response = await http.post("${Constantes.urlAPI}/signin", body: data);
    print(response);
    if(response.statusCode == 200){
      jsonResponse = json.decode(response.body);

      print('Response status: ${response.statusCode}');
      print('Response body: ${response.body}');

      if(jsonResponse != null){
        setState(() {
        _isLoading = false;
      });
      sharedPreferences.setString("token", jsonResponse['token']);
      
      //Cambiar de navegacion
      Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (BuildContext context) => MainPage()), (Route<dynamic> route) => false);
      //Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (BuildContext context) => MyMovieApp()), (Route<dynamic> route) => false);
      
      }
    }
    else {
      setState(() {
        _isLoading = false;
      });
      print(response.body);
    }
  }

  Container butonSection(){
    return Container(
      // width: SizeConfig.blockSizeHorizontal * 5,//
      height: SizeConfig.blockSizeVertical * 9,
      padding:  EdgeInsets.symmetric(horizontal: 50.0),
      child: RaisedButton(
        onPressed: () {
          setState(() {
            _isLoading = true;
          });
          signIn(emailController.text, passwordController.text);
        },
        elevation:  0.0,
        color: Colors.black,
        child: Text("Login", style: TextStyle(color:  Colors.white70)),
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(50.0)),
      ),
    );
  }

    Container butonOlviContra(){
    return Container(
      // width: SizeConfig.blockSizeHorizontal * 5,//
      height: SizeConfig.blockSizeVertical * 9,
      padding:  EdgeInsets.symmetric(horizontal: 50.0),
      child: RaisedButton(
        onPressed: () {
          /*setState(() {
            _isLoading = true;
          });
          signIn(emailController.text, passwordController.text);*/
        },
        elevation:  0.0,
        color: Colors.black,
        child: Text("Olvide Contraseña", style: TextStyle(color:  Colors.white70)),
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(50.0)),
      ),
    );
  }

  Container textSection(){
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 15.0, vertical: 20.0),
      child: Column(
        children: <Widget>[
          TextField(
            controller: emailController,
            cursorColor: Colors.white,
            style: TextStyle(color: Colors.white70),
            decoration: InputDecoration(
              icon: Icon(Icons.email, color: Colors.white70),
              hintText: "Usuario/Correo",
              border: UnderlineInputBorder(borderSide: BorderSide(color: Colors.white70)),
              hintStyle: TextStyle(color: Colors.white)
            ),
          ),
          SizedBox(height: 30.0,),

          TextField(
            controller: passwordController,
            cursorColor: Colors.white,
            style: TextStyle(color: Colors.white70),
            decoration: InputDecoration(
              icon: Icon(Icons.lock, color: Colors.white70),
              hintText: "Clave",
              border: UnderlineInputBorder(borderSide: BorderSide(color: Colors.white70)),
              hintStyle: TextStyle(color: Colors.white)
            ),obscureText: true,
          ),

        ],
      ),
    );
  }

  Container heardSection() {
    return Container(
      margin: EdgeInsets.only(top: 50.0),
      padding: EdgeInsets.symmetric(horizontal: 20.0, vertical: 30.0,
    ),
    //child: Text("BacanalApp",style: TextStyle(color: Colors.white70,fontSize: 40.0,fontWeight: FontWeight.bold)),
    child: RichText(textAlign: TextAlign.center,
                      text: TextSpan(
                        text: 'BacanalApp ',
                        style: TextStyle(color: Colors.white70,fontSize: 40.0,fontWeight: FontWeight.bold, fontFamily: 'Karla'),
                        children: [TextSpan(text: '',style: TextStyle(fontSize: 20.0, fontFamily: 'Karla')),],
                      ),
                    ),
    );
  }
  
}