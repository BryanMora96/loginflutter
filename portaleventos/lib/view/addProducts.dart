import 'package:portaleventos/controllers/databasehelpers.dart';
import 'package:portaleventos/main.dart';
import 'package:flutter/material.dart';

class AddDataProduct extends StatefulWidget {
  AddDataProduct({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _AddDataProductState createState() => _AddDataProductState();
}

class _AddDataProductState extends State<AddDataProduct> {
  DataBaseHelper databaseHelper = new DataBaseHelper();

  final TextEditingController _nameController = new TextEditingController();
  final TextEditingController _priceController = new TextEditingController();
  final TextEditingController _stockController = new TextEditingController();

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Add Product',
      theme: ThemeData.dark(),
      home: Scaffold(
        appBar: AppBar(
          title: Text('Adicionar Evento'),
        ),
        body: Container(
          child: ListView(
            padding: const EdgeInsets.only(
                top: 62, left: 12.0, right: 12.0, bottom: 12.0),
            children: <Widget>[
              Container(
                height: 50,
                child: new TextField(
                  controller: _nameController,
                  keyboardType: TextInputType.emailAddress,
                  decoration: InputDecoration(
                    labelText: 'Nombre del Evento',
                    hintText: 'Nombre del Evento',
                    icon: new Icon(Icons.email),
                  ),
                ),
              ),
              Container(
                height: 50,
                child: new TextField(
                  controller: _priceController,
                  keyboardType: TextInputType.text,
                  decoration: InputDecoration(
                    labelText: 'Precio del Evento',
                    hintText: 'Precio del Evento',
                    icon: new Icon(Icons.vpn_key),
                  ),
                ),
              ),
              /*new Padding(
                padding: new EdgeInsets.only(top: 44.0),
              ),*/
              Container(
                height: 50,
                child: new TextField(
                  controller: _stockController,
                  keyboardType: TextInputType.text,
                  decoration: InputDecoration(
                    labelText: 'Catidad de Personas',
                    hintText: 'Catidad de Personas',
                    icon: new Icon(Icons.vpn_key),
                  ),
                ),
              ),
              new Padding(
                padding: new EdgeInsets.only(top: 44.0),
              ),
              Container(
                height: 50,
                child: new RaisedButton(
                  onPressed: () {
                    databaseHelper.addDataProducto(
                        _nameController.text.trim(),
                        _priceController.text.trim(),
                        _stockController.text.trim());
                    Navigator.of(context).push(new MaterialPageRoute(
                      builder: (BuildContext context) => new MainPage(),
                    ));
                  },
                  color: Colors.black,
                  child: new Text(
                    'Agregar Evento',
                    style: new TextStyle(
                        color: Colors.white, backgroundColor: Colors.black),
                  ),
                  shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(50.0)),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
