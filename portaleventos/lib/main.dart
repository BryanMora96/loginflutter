import 'package:flutter/material.dart';
import 'package:portaleventos/utilities/size_config.dart';
import 'package:portaleventos/view/addProducts.dart';
import 'package:portaleventos/view/listProducts.dart';
import 'package:portaleventos/view/loginPage.dart';
import 'package:portaleventos/view/viewEvent.dart';
import 'package:shared_preferences/shared_preferences.dart';
//import 'package:flutter_secure_storage/flutter_secure_storage.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'App Nodejs Mongodb',
      /*theme: ThemeData(primarySwatch: Colors.blue,),*/
      theme: ThemeData.dark(),
      //home: MainPage(),
      home: LoginPage(),
    );
  }
}

class MainPage extends StatefulWidget {
  @override
  _MainPageState createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {
  SharedPreferences sharedPreference;

  @override
  void initState() {
    super.initState();
    checkLoginStatus();
  }

  //chequea el estao del login, cuando inicia la app comprueba el estado, si ha sido logueado lo dejara continuar sin pedir usuario y contraseña, en caso contrario le pedira usario y contraseña
  checkLoginStatus() async {
    sharedPreference = await SharedPreferences.getInstance();
    if (sharedPreference.getString("token") == null) {
      Navigator.of(context).pushAndRemoveUntil(
          MaterialPageRoute(builder: (BuildContext context) => LoginPage()),
          (Route<dynamic> route) => false);
    }
  }

  Container butonSection() {
    return Container(
      // width: SizeConfig.blockSizeHorizontal * 5,//
      height: SizeConfig.blockSizeVertical * 10,
      padding: EdgeInsets.symmetric(horizontal: 15.0),
      child: RaisedButton(
        onPressed: () {
          /*setState(() {
            _isLoading = true;
          });
          signIn(emailController.text, passwordController.text);*/
        },
        elevation: 0.0,
        color: Colors.black,
        child: Text("Guardar", style: TextStyle(color: Colors.white70)),
        shape:
            RoundedRectangleBorder(borderRadius: BorderRadius.circular(50.0)),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
      appBar: AppBar(
        title: Text("Primera Pantalla", style: TextStyle(color: Colors.white)),
        actions: <Widget>[
          FlatButton(
            onPressed: () {
              sharedPreference.clear();
              sharedPreference.commit();
              Navigator.of(context).pushAndRemoveUntil(
                  MaterialPageRoute(
                      builder: (BuildContext context) => LoginPage()),
                  (Route<dynamic> route) => false);
            },
            child: Text(
              "Logout",
              style: TextStyle(color: Colors.white),
            ),
          )
        ],
      ),
      //body: Center(child: Text("Main page")),
      body: Container(
        padding: EdgeInsets.symmetric(horizontal: 15.0, vertical: 20.0),
        child: Column(
          children: <Widget>[
            TextField(
              // controller: emailController,
              cursorColor: Colors.white,
              style: TextStyle(color: Colors.white70),
              decoration: InputDecoration(
                  icon: Icon(Icons.face, color: Colors.white70),
                  hintText: "Nombres",
                  border: UnderlineInputBorder(
                      borderSide: BorderSide(color: Colors.white70)),
                  hintStyle: TextStyle(color: Colors.white)),
            ),
            SizedBox(
              height: 30.0,
            ),
            TextField(
              //controller: passwordController,
              cursorColor: Colors.white,
              style: TextStyle(color: Colors.white70),
              decoration: InputDecoration(
                  icon: Icon(Icons.face, color: Colors.white70),
                  hintText: "Apellidos",
                  border: UnderlineInputBorder(
                      borderSide: BorderSide(color: Colors.white70)),
                  hintStyle: TextStyle(color: Colors.white)),
            ),
            SizedBox(
              height: 30.0,
            ),
            TextField(
              //controller: passwordController,
              cursorColor: Colors.white,
              style: TextStyle(color: Colors.white70),
              decoration: InputDecoration(
                  icon: Icon(Icons.email, color: Colors.white70),
                  hintText: "Correo",
                  border: UnderlineInputBorder(
                      borderSide: BorderSide(color: Colors.white70)),
                  hintStyle: TextStyle(color: Colors.white)),
            ),
            SizedBox(
              height: 30.0,
            ),
            TextField(
              //controller: passwordController,
              cursorColor: Colors.white,
              style: TextStyle(color: Colors.white70),
              decoration: InputDecoration(
                  icon: Icon(Icons.lock, color: Colors.white70),
                  hintText: "Clave",
                  border: UnderlineInputBorder(
                      borderSide: BorderSide(color: Colors.white70)),
                  hintStyle: TextStyle(color: Colors.white)),
            ),
            SizedBox(
              height: 30.0,
            ),
            butonSection(),
          ],
        ),
      ),

      drawer: Drawer(
        child: new ListView(
          children: <Widget>[
            new UserAccountsDrawerHeader(
              accountName: new Text("Nodejs"),
              accountEmail: new Text("micorreo@gmail.com"),
            ),
            new ListTile(
              title: new Text("Ver Peliculas"),
              trailing: new Icon(Icons.list),
              onTap: () => Navigator.of(context).push(new MaterialPageRoute(
                builder: (BuildContext context) => MyMovieApp(),
              )),
            ),
            new ListTile(
              title: new Text("Ver Tickets"),
              trailing: new Icon(Icons.list),
              onTap: () => Navigator.of(context).push(new MaterialPageRoute(
                builder: (BuildContext context) => ListProducts(),
              )),
            ),
            new ListTile(
              title: new Text("Pagos Realizados"),
              trailing: new Icon(Icons.add),
              onTap: () {},
            ),
            new ListTile(
              title: new Text("Eventos Favoritos"),
              trailing: new Icon(Icons.add),
              onTap: () {},
            ),
            new ListTile(
              title: new Text("Crear Eventos"),
              trailing: new Icon(Icons.add),
              onTap: () => Navigator.of(context).push(new MaterialPageRoute(
                builder: (BuildContext context) => AddDataProduct(),
              )),
            ),
          ],
        ),
      ),
    );
  }
  //Modulo Vista de eventos
}
